import java.io.*;   
import java.net.*;  
import java.util.Scanner;

/**
 * @author Snilledata
 */
public class ATMClient {
	
	private static int connectionPort = 7037;
	static PrintWriter out = null;
	static BufferedReader in = null;
	
	/**
	 * This function reads from the server by reading one line at a time for "length" times.
	 * The variable length is always the first thing that the server will write to the client.
	 * @return [description]
	 */
	public static String getMessage() {
		String message = "";
		try {
			String tmp = "";
			int length = Integer.parseInt(in.readLine());
			for(int i=0; i<length; i++) {
				tmp = in.readLine();
				message += tmp;
			}
		
		} catch (IOException e) {
			System.exit(1);
		}
		return message;
	}

	/**
	 * Sends a message to the server. It splits the string in pieces of 2 chars at a time.
	 * @param s [description]
	 */
	public static void sendMessage(String s) {
		int count = 2;
		String[]sa = s.split("(?<=\\G.{" + count + "})");
		out.println(sa.length);
        for(String i : sa) {
        	out.println(i);
        }
	}
	
	public static void main(String[] args) throws IOException {
		final String VALID = "1";
		final String CHANGE_WELCOME_MESSAGE_TRUE = "c";
		final String CODE_CHECK = "cc";
		final String INVALID_CODE = "nc";
	
		Socket ATMSocket = null;
		String adress = "";

		/**
		 * Connect to the server.
		 */
		try {
			adress = args[0];
		} catch (ArrayIndexOutOfBoundsException e) {
			System.err.println("Missing argument ip-adress");
			System.exit(1);
		}
		try {
			ATMSocket = new Socket(adress, connectionPort); 
			out = new PrintWriter(ATMSocket.getOutputStream(), true);
			in = new BufferedReader(new InputStreamReader
					(ATMSocket.getInputStream()));
		} catch (UnknownHostException e) {
			System.err.println("Unknown host: " +adress);
			System.exit(1);
		} catch (IOException e) {
			System.err.println("Couldn't open connection to " + adress);
			System.exit(1);
		}        
		Scanner scanner = new Scanner(System.in);
		String answer = "";
		
		/**
		 * While the server doesn't send VALID to the client we will loop a string.
		 * In natural language it means that the server doesn't recognize the username
		 */
		while(!answer.equals(VALID)) { //Valid user
			System.out.println(getMessage()); // System.out.println("Enter username");
			System.out.print("> ");
			int user = scanner.nextInt(); // Scanning for the user
			out.println(user);
			answer = in.readLine();
		}
		answer = getMessage();
		/**
		 * The while loop below behaves just like the while loop above.
		 */
		while(!answer.equals(VALID)) { //Valid password
			System.out.println(answer); //System.out.println("Enter password");
			System.out.print("> ");
			int pass = scanner.nextInt();
			out.println(pass);
			answer = getMessage();
		}

		System.out.println("Contacting bank ... ");
		System.out.println(getMessage()); 	
		/**
		 * We are now logged in and the welcome message has just been printed.
		 */
		System.out.print("> ");
		int menuOption = scanner.nextInt();
		int userInput;
		out.println(menuOption);
		/**
		 * Depending on the menuoption different procedures will happen.
		 */
		while(menuOption < 10) {
			if(menuOption == 5) {
				//LANGUAGE CHANGE
				String question = getMessage(); // Which language do you wish to change to?
				System.out.println(question);
				String options = getMessage();
				System.out.println(options);
				scanner.nextLine();
				System.out.print("> ");
				userInput = scanner.nextInt();
				out.println(userInput);
				System.out.println(getMessage()); // MENY
				System.out.print("> ");
				menuOption = scanner.nextInt();
				out.println(menuOption);
			} else if(menuOption == 9) {
				//CHANGING WELCOME STRING.
				String response = getMessage(); //MESSAGE_TRUE eller You are not a super
				String message = getMessage(); //Change welcome message or (message + " (1)Balance, (2)Withdrawal, (3)Deposit, (4)Exit
				/**
				 * If the server allows the current user to change the welcome string response will equal CHANGE_WELCOME_MESSAGE_TRUE
				 */
				if(response.equals(CHANGE_WELCOME_MESSAGE_TRUE)) {
					System.out.println(message);
					scanner.nextLine();
					System.out.print("> ");
					String newMessage = scanner.nextLine();
					sendMessage(newMessage);
					System.out.println(getMessage());
					System.out.println(getMessage());
				} else {
					/**
					 * The server responded that you are not sudo and are not allowed to modify the welcome string.
					 */
					System.out.println(response);
					System.out.println(message);
				}
				/**
				 * Print the prompt and scan for a new menu option.
				 */
				System.out.print("> ");
				menuOption = scanner.nextInt();
				out.println(menuOption); 
			} else if(menuOption == 1) {
				//CHECK BALANCE
				System.out.println(getMessage()); 
				System.out.println(getMessage());
				System.out.print("> ");
				menuOption = scanner.nextInt();
				out.println(menuOption);           
			} else if (menuOption > 3) {
				break;
			}	
			else {
				/**
				 * The user wants to either deposit or withdraw money. If the server
				 * prints CODE_CHECK to the client the client will ask for a verification code.
				 * Otherwise the user is asked to enter the amount to deposit.
				 */
				String str = getMessage();
				String message = getMessage();
				if(str.equals(CODE_CHECK)) { //ENTER VERIFICATION CODE.
					System.out.println(message);
					scanner.nextLine(); 
					System.out.print("> ");
					String code = scanner.nextLine();
					out.println(code);
					// Skanna kod
					String s = getMessage();
					if(s.equals(INVALID_CODE)) {
						/**
						 * Invalid code. The user will not get a second chance. Instead the menu will be printed.
						 */
						System.out.println(getMessage()); //INVALID
						System.out.println(getMessage()); //MENY
					} else {
						System.out.println(getMessage()); // Enter amount
						System.out.print("> ");
						userInput = scanner.nextInt();
						out.println(userInput);
						System.out.println(getMessage());
						System.out.println(getMessage());
					}
				} else {
					//Enter amount to deposit!
					System.out.println(str); 
					/**
					 * The menu is printed and the user can enter another menu option.
					 */
					System.out.print("> ");
					userInput = scanner.nextInt();
					out.println(userInput);
					System.out.println(getMessage());
					System.out.println(getMessage());
				}
				System.out.print("> ");
				menuOption = scanner.nextInt();
				out.println(menuOption); 
			}	
		}
		System.out.println(getMessage());
		out.close();
		in.close();
		ATMSocket.close();
	}
}   
