import java.io.*;
import java.util.*;
public class Account implements java.io.Serializable
{
	public String username = "8";
	public String password = "1234";
	public int balance = 1100;
	public int sudo = 0; //superuser.
	public Map<String, Boolean> codes;
	
	public Account() {

		/**
		 * Alla konton har samma set av engångskoder.
		 */
		codes = new HashMap<String, Boolean>();
		codes.put("01", true);
		codes.put("03", true);
		codes.put("05", true);
		codes.put("07", true);
		codes.put("09", true);
		codes.put("11", true);
		codes.put("13", true);
		codes.put("15", true);
		codes.put("17", true);
		codes.put("19", true);
		codes.put("21", true);
		codes.put("23", true);
		codes.put("25", true);
		codes.put("27", true);
		codes.put("29", true);
		codes.put("31", true);
		codes.put("33", true);
		codes.put("35", true);
		codes.put("37", true);
		codes.put("39", true);
		codes.put("41", true);
		codes.put("43", true);
		codes.put("45", true);
		codes.put("47", true);
		codes.put("49", true);
		codes.put("51", true);
		codes.put("53", true);
		codes.put("55", true);
		codes.put("57", true);
		codes.put("59", true);
		codes.put("61", true);
		codes.put("63", true);
		codes.put("65", true);
		codes.put("67", true);
		codes.put("69", true);
		codes.put("71", true);
		codes.put("73", true);
		codes.put("75", true);
		codes.put("77", true);
		codes.put("79", true);
		codes.put("81", true);
		codes.put("83", true);
		codes.put("85", true);
		codes.put("87", true);
		codes.put("89", true);
		codes.put("91", true);
		codes.put("93", true);
		codes.put("95", true);
		codes.put("97", true);
		codes.put("99", true);
	}

	public static void main(String[] args) {
		Account a = new Account();
		try
		{
			FileOutputStream fileOut =
				new FileOutputStream("8.ser");
			ObjectOutputStream out = new ObjectOutputStream(fileOut);
			out.writeObject(a);
			out.close();
			fileOut.close();
			System.out.printf("Serialized data is saved in 7.ser");
		}catch(IOException i)
		{
			i.printStackTrace();
		}
	}
}
