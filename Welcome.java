import java.io.*;
public class Welcome implements java.io.Serializable
{
	public String welcomeString = "Welcome to Bank!";

	public Welcome() {
	}


	public void serialize() {

	}

	public static void main(String[] args) {
		Welcome a = new Welcome();
		try
		{
			FileOutputStream fileOut =
				new FileOutputStream("welcome.ser");
			ObjectOutputStream out = new ObjectOutputStream(fileOut);
			out.writeObject(a);
			out.close();
			fileOut.close();
			System.out.printf("Serialized data is saved in /welcome.ser");
		}catch(IOException i)
		{
			i.printStackTrace();
		}
	}
}
