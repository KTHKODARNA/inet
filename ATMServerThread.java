import java.io.*;
import java.net.*;

/**
 * @author Viebrapadata
 */
public class ATMServerThread extends Thread {
	Account a;
	Welcome w;
	String filename = "";
	private Socket socket = null;
	private int maxLength = 80;
	private static BufferedReader in;
	PrintWriter out;
	private String message;
	private int balance;
	final String VALID = "1";
	final String CHANGE_WELCOME_MESSAGE_TRUE = "c";
	final String CODE_CHECK = "cc";
	final String INVALID_CODE = "nc";
	int language = 1; //0 = eng, 1 = sve
	/**
	 * messageMatrix contains translations for standard strings that the client will see. 
	 * Each element in the matrix contains its corresponding translation. The 0:th element is "Enter username" which translates to "Skriv in användarnamn".
	 * Adding a new language is easy. You simply add another set of strings where each string translates correctly. Only this class needs to be altered! 
	 */
	String[][] messageMatrix = {{"Enter username", "Enter password", "Invalid password", "Enter verification code", "Invalid verification code", "(1)Balance, (2)Withdrawal, (3)Deposit, (4)Exit, (5)Choose language", "Which language do you wish to change to?", "(1)ENG, (2)SV", "You are not a super user", "Change welcome message", "Welcome message successfully updated", "Enter amount: ", "Current balance is ", " dollars", "Good Bye", VALID, CHANGE_WELCOME_MESSAGE_TRUE, CODE_CHECK, INVALID_CODE},
								{"Skriv in användarnamn", "Skriv in lösenord", "Ogiltigt lösenord", "Ange engångskod", "Ogiltig engångskod", "(1)Saldo, (2)Uttag, (3)Insättning, (4)Avsluta, (5)Välj språk", "Vilket språk vill du byta till?", "(1)ENG, (2)SV", "Otillåtet val!", "Ändra välkomststräng", "Välkomststrängen är uppdaterad", "Ange belopp ", "Nuvarande saldo ", " dollar", "Hej då", VALID, CHANGE_WELCOME_MESSAGE_TRUE, CODE_CHECK, INVALID_CODE}}; 

	public ATMServerThread(Socket socket) {
		super("ATMServerThread");
		this.socket = socket;
	}


	private String readLine() throws IOException {
		String str = in.readLine();
		//System.out.println(""  + socket + " : " + str);
		return str;
	}
	
	/**
	 * sendTenBytes is used for the server to communicate with the client.
	 * the input param message describes which element in the messageMatrix we are interested in printing to the client.
	 * Depending on the variable language the correct string will be chosen.
	 *
	 * By altering the count variable we can decide how many bytes we wish to send at a time. (1 char = 2 byte, a char in java is encoded in UTF-16).  
	 * If count = x then the string will be split in slices of x chars. 
	 */
	public void sendTenBytes(int message) {
		String s = "";
		if(message == 5) {
			s += this.message;
			s += " ";
		}
		if(message == 12) {
			s += messageMatrix[language][message];
			s += balance;
			message = 13;
		}
		int count = 2;
		s += messageMatrix[language][message];
		String[]sa = s.split("(?<=\\G.{" + count + "})");
		out.println(sa.length);
        for(String i : sa) {
            out.println(i);
        }
	}

	public String getMessage() {
		String message = "";
		try {
			String tmp = "";
			int length = Integer.parseInt(in.readLine());
			System.out.println(length);
			for(int i=0; i<length; i++) {
				tmp = in.readLine();
				message += tmp;
			}
		
		} catch (IOException e) {
			System.out.println("FEL");
			System.exit(1);
		}
		return message;
	}

	/**
	 * Helper function that uses serializalition to check if the entered user exists.
	 * It communicates with the client and asks for the correct password. This goes on until a correct password has been supplied.  
	 */
	private boolean validateUser() throws IOException {
		sendTenBytes(0);
		int user = Integer.parseInt(readLine());
		filename = "" + user + ".ser";
		try {
			FileInputStream fileIn = new FileInputStream(filename);
			ObjectInputStream in = new ObjectInputStream(fileIn);
			a = (Account) in.readObject();
			in.close();
			fileIn.close();
		} 
		catch(IOException i)
		{
			i.printStackTrace();
			out.println("User " + user + " not found");
			return false;
		}
		catch(ClassNotFoundException c) {
			out.println("User " + user + " not found");
			c.printStackTrace();
			return false;
		}
		
		// user existerar
		out.println(VALID);
		sendTenBytes(1);
		String pass = readLine();
		while(!pass.equals(a.password)) {
			sendTenBytes(2);
			pass = readLine();
		}
		sendTenBytes(15); //
		return true;
	}

	/**
	 * Uses serialization to read from welcome.ser and updates welcomeString to the supplied message.
	 * If the message is longer than 80 chars the updated string will be cut so that it only keeps the first 80 chars. 
	 */
	public boolean updateMessage() {
		try {
			FileInputStream fileIn = new FileInputStream("welcome.ser");
			ObjectInputStream fin = new ObjectInputStream(fileIn);
			w = (Welcome) fin.readObject();
			fin.close();
			fileIn.close();
		} 
		catch(IOException i)
		{
			i.printStackTrace();
			return false;
		}
		catch(ClassNotFoundException c) {
			System.out.println("class not found");
			c.printStackTrace();
			return false;
		}
		message = w.welcomeString;
		if (message.length() > maxLength) {
			message = message.substring(0, maxLength);
		}
		return true;
	}
	
	/**
	 * This helps us validate a verification code. It communicates with the client by asking for a verification code.
	 * Depending on if it is a valid code it will either notify the client that the verification code is correct, OR notify that its a valid code. If the code was valid it is removed from the list of correct codes.
	 */
	public boolean validateCode() throws IOException {
		sendTenBytes(17); //out.println(CODE_CHECK);
		sendTenBytes(3);
		//out.println("Enter verification code");
			
		// Läs in kod från client
		String pass = readLine();
		// Checka koden
		Boolean used;
		try {
			used = a.codes.get(pass);
		} catch(NullPointerException e) {
			sendTenBytes(18); // INVALID CODE
			sendTenBytes(4); // INVALID VERIFICATION CODE
			return false;
		}
		// Kolla om used är true eller false!!!!
		if(used == null) {
			sendTenBytes(18); // INVALID CODE
			sendTenBytes(4); // INVALID VERIFICATION CODE
			return false;
		}
		if(used == false) {
			sendTenBytes(18); // INVALID CODE
			sendTenBytes(4); // INVALID VERIFICATION CODE
			return false;
		}
		a.codes.put(pass, false);
		sendTenBytes(15);
		
		return true;
		
	}

	/**
	 * Runs whenever we create a new ATMServerThread.
	 */
	public void run(){

		try {
			out = new PrintWriter(socket.getOutputStream(), true);
			in = new BufferedReader
				(new InputStreamReader(socket.getInputStream()));

			String inputLine, outputLine;
			/**
			 * the user must be validated (logged in) to continue.
			 */
			while(!validateUser()){
			}
			/**
			 * The users balance will be read from the users serializiable file.
			 * The menu will be printed and the user will be able to choose a menu option
			 */
			updateMessage();
			balance = a.balance;
			int value;
			sendTenBytes(5); //MENY
			inputLine = readLine();
			int choice = Integer.parseInt(inputLine);
			while (choice != 4) {
				int deposit = 1;
				updateMessage();
				switch (choice) {
					case 5:
						//CHANGE LANGUAGE
						sendTenBytes(6);
						sendTenBytes(7);
						int langChoice = Integer.parseInt(readLine());
						//The variable language decides which array in messageMatrix that we read from.
						language = langChoice - 1;
						
						if(language > messageMatrix.length) {
							language = 0;
						}
						sendTenBytes(5);
						inputLine=readLine();
						choice = Integer.parseInt(inputLine);
						break;
					case 9:
						//CHANGE WELCOME MESSAGE
						if(a.sudo == 0) // not super user
						{
							sendTenBytes(8); //out.println("You are not a super user");
							sendTenBytes(5); //out.println(message + " (1)Balance, (2)Withdrawal, (3)Deposit, (4)Exit");
							inputLine=readLine();
							System.out.println(inputLine);
							choice = Integer.parseInt(inputLine);
							break;
						} else {
							sendTenBytes(16); //out.println(CHANGE_WELCOME_MESSAGE_TRUE);
							sendTenBytes(9); //out.println("Change welcome message");
							//inputLine = readLine(); //READ THE NEW SUPPLIED MESSAGE.
							inputLine= getMessage(); 
							try
							{
								Welcome w = new Welcome();
								w.welcomeString = inputLine;
								FileOutputStream fileOut = new FileOutputStream("welcome.ser");
								ObjectOutputStream fout = new ObjectOutputStream(fileOut);
								fout.writeObject(w);
								fout.close();
								fileOut.close();
								sendTenBytes(10); //out.println("Welcome message successfully updated");
							}catch(IOException i)
							{
								i.printStackTrace();
							}
							updateMessage();
							sendTenBytes(5);
							// out.println(message + " (1)Balance, (2)Withdrawal, (3)Deposit, (4)Exit");
							inputLine=readLine();
							choice = Integer.parseInt(inputLine);
							break;
						}
					case 2:
						//WITHDRAW
						deposit = -1;
						// Kolla engångskod
						if(!validateCode()) {
							sendTenBytes(5); // out.println(message + " (1)Balance, (2)Withdrawal, (3)Deposit, (4)Exit");
							inputLine=readLine();
							choice = Integer.parseInt(inputLine);
							break;
						}
					case 3:
						//DEPOSIT
						sendTenBytes(11); // out.println("Enter amount: ");
						if(choice == 3) {
							sendTenBytes(11);
						}
						inputLine= readLine();
						value = Integer.parseInt(inputLine);
						balance += deposit * value;
					case 1:
						//CHECK BALANCE
						sendTenBytes(12); // out.println("Current balance is " + balance + " dollars");
						sendTenBytes(5); // out.println(message + " (1)Balance, (2)Withdrawal, (3)Deposit, (4)Exit");
						inputLine=readLine();
						choice = Integer.parseInt(inputLine);
						break;
					case 4:
						break;
					default: 
						break;
				}
			}
			try
			{
				a.balance = balance;
				FileOutputStream fileOut = new FileOutputStream(filename);
				ObjectOutputStream out = new ObjectOutputStream(fileOut);
				out.writeObject(a);
				out.close();
				fileOut.close();
				//System.out.printf("Serialized data is saved in " + a.username + ".ser");
			}catch(IOException i)
			{
				i.printStackTrace();
			}
			sendTenBytes(14); //out.println("Good Bye");
			
			out.close();
			in.close();
			socket.close();
		}catch (IOException e){
			e.printStackTrace();
		}

	}

}
